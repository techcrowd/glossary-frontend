import React from "react";
import logo from "./logo.svg";
import "./App.scss";
//import Navbar from "./components/Navigation/Navbar";
import {
  Button,
  Accordion,
  Card,
  Navbar,
  Dropdown,
  ButtonGroup,
  NavDropdown,
  Nav,
  Form,
  FormControl,
  InputGroup
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

const App: React.FC = () => {
  return (
    <div className="App">
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">Glossory</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <NavDropdown title="View Users" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">
                Active Users
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Pending Users
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">
                Rejected Users
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Form inline>
            <InputGroup>
              <FormControl
                placeholder="Search User Glossory"
                aria-label="Search User Glossory"
                aria-describedby="basic-addon2"
              />
              <InputGroup.Append>
                <Button variant="outline-dark">Search</Button>
              </InputGroup.Append>
            </InputGroup>
            s p a c e<Button variant="outline-dark">Login</Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
      <header className="App-header">
        <div className="glossory-landing-page">
          <h2>View Users</h2>
          <Accordion defaultActiveKey="0">
            <Card>
              <Accordion.Toggle as={Card.Header} eventKey="0">
                Approved
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="0">
                <Card.Body>
                  Hello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the body
                </Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Accordion.Toggle as={Card.Header} eventKey="1">
                Pending
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="1">
                <Card.Body>
                  Hello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the body
                </Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Accordion.Toggle as={Card.Header} eventKey="2">
                Rejected
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="2">
                <Card.Body>
                  Hello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the bodyHello! I'm the bodyHello! I'm the
                  bodyHello! I'm the body
                </Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </div>
      </header>
    </div>
  );
};

export default App;
