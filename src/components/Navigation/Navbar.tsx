import { Component } from "react";
import React from "react";
import "./style.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Dropdown } from "react-bootstrap";

export default class NavBar extends Component<any> {
  render() {
    return (
      <div className="Navbar">
        <Button variant="primary" className="navBtn">
          View Users <img src={require("../../images/dropDownIcon.svg")} />
        </Button>

        <div className="navBtn">
          Login <img src={require("../../images/loginIcon.svg")} />
        </div>
      </div>
    );
  }
}
